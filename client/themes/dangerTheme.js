export default {
   primary: '#F85359',
   secondary: '#424242',
   accent: '#82B1FF',
   error: '#F7981C',
   info: '#09C199',
   success: '#39B54A',
   warning: '#1E3C72'
}