export default {
   primary: '#F7981C',
   secondary: '#424242',
   accent: '#82B1FF',
   error: '#F85359',
   info: '#09C199',
   success: '#39B54A',
   warning: '#1E3C72'
}