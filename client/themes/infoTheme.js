export default {
   primary: '#09C199',
   secondary: '#424242',
   accent: '#82B1FF',
   error: '#F85359',
   info: '#1E3C72',
   success: '#39B54A',
   warning: '#F7981C'
}