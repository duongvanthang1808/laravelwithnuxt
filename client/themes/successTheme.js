export default {
   primary: '#39B54A',
   secondary: '#424242',
   accent: '#82B1FF',
   error: '#F85359',
   info: '#09C199',
   success: '#1E3C72',
   warning: '#F7981C'
}