export default {
   primary: '#82B1FF',
   secondary: '#424242',
   accent: '#1E3C72',
   error: '#F85359',
   info: '#09C199',
   success: '#39B54A',
   warning: '#F7981C'
}