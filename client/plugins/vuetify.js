import Vue from 'vue'
import Vuetify from 'vuetify'
import primaryTheme from '~/themes/primaryTheme';

Vue.use(Vuetify, {
    theme: primaryTheme
});