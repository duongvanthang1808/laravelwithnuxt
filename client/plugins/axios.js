import axios from 'axios'
import swal from 'sweetalert2'
import NProgress from 'nprogress'

NProgress.configure({
    easing: 'linear',
    speed: 350
});

process.env.NODE_TLS_REJECT_UNAUTHORIZED = '0'

export default ({app, store, redirect}) => {
    if (process.server) {
        return
    }

    // Request interceptor
    axios.interceptors.request.use(request => {
        const token = store.getters['auth/token']
        const user = store.getters['auth/user']

        request.baseURL = process.env.apiUrl

        if (process.env.apiPrefix) {
            request.baseURL += '/' + process.env.apiPrefix
        }

        request.baseURL += process.env.prefix

        if (token) {
            request.headers.common['Authorization'] = `Bearer ${token}`;
        } else {
            request.data['grant_type'] = 'password'
            request.data['client_id'] = process.env.client_id
            request.data['client_secret'] = process.env.client_secret
        }

        const locale = store.getters['lang/locale']

        if (locale) {
            request.headers.common['Accept-Language'] = locale
        }

        console.log(request.baseURL)

        return request
    })

    // Response interceptor
    axios.interceptors.response.use(response => response, error => {
        const {status} = error.response || {}

        if (status >= 500) {
            swal({
                type: 'error',
                title: app.i18n.t('error_alert_title'),
                text: app.i18n.t('error_alert_text'),
                reverseButtons: true,
                confirmButtonText: app.i18n.t('ok'),
                cancelButtonText: app.i18n.t('cancel')
            })
        }

        if (status === 401 && store.getters['auth/check']) {
            swal({
                type: 'warning',
                title: app.i18n.t('token_expired_alert_title'),
                text: app.i18n.t('token_expired_alert_text'),
                reverseButtons: true,
                confirmButtonText: app.i18n.t('ok'),
                cancelButtonText: app.i18n.t('cancel')
            }).then(() => {
                store.commit('auth/LOGOUT')

                redirect({name: 'login'})
            })
        }

        return Promise.reject(error)
    })

    axios.defaults.transformRequest.push(function (data, headers) {
        NProgress.start();
        return data;
    })

    axios.defaults.transformResponse.push(function (data, headers) {
        NProgress.done();
        return data;
    })
}
