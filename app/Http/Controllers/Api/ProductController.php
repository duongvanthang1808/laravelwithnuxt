<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Product\ProductRepositoryInterface;

class ProductController extends Controller
{
    /**
     * @author thangdv8182
     * @param ProductRepositoryInterface $product
     */
    public function __construct(ProductRepositoryInterface $product)
    {
        $this->product = $product;
    }
    public function index(Request $request) {
        $products = $this->product->all();
        return response()->json([
            'datas' => $products,
            'total' => $products->count(),
            'total_pages' => 3,
            'status' => '200',
        ]);
    }
}
