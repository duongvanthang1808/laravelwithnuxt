<?php
namespace App\Repositories\Product;

use App\Models\Product;

/**
 *
 * @author tampt6722
 *
 */
class ProductRepository implements ProductRepositoryInterface
{

    public function all(){
        return Product::all();
    }

    public function paginate($quantity){
        return Product::paginate($quantity);
    }

    public function find($id){
        return Product::find($id);
    }

    public function save($data){
        return Product::create($data);
    }

    public function delete($id){
        Product::find($id)->delete();
        return true;
    }

    public function update($data, $id){
        $product = Product::find($id);
        $product->save($data);
        return true;
    }
}